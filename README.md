In deze opdracht moet u zelf een aantal klassen maken. U dient
constructoren en methoden met eenvoudige algoritmiek te schrijven.
De opdracht betreft een kleine bibliotheek en is een wat uitgebreidere en
gewijzigde versie van de bibliotheek die aan het eind van leereenheid 6 is
behandeld.
De applicatie kent vijf klassen: Bibliotheek, Boek, Exemplaar, Auteur en
Biblio.
Hierna volgen de beschrijvingen van iedere klasse. Indien niet expliciet
aangegeven beschikken de klassen over constructoren en get-methoden.
