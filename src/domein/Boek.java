package domein;


/**
 * Klasse die verantwoordelijk is voor het generen van boeken. 
 * @author youhao
 *
 */
public class Boek {
	private String titel = null;
	private String taal = null;
	private Auteur auteur;
	

	/**
	 * Maakt een nieuwe Boek aan met gegeven titel, taal en referentie
	 * naar Auteur.
	 * @param titel de titel van het boek
	 * @param taal de taal van het boek
	 * @param auteur de referentie naar auteur object
	 */
	
	public Boek (String titel, String taal, Auteur auteur) {
		this.titel = titel;
		this.taal = taal;
		this.auteur = auteur;
	}
	

	/**
	 * Maakt een nieuwe Boek aan met gegeven titel en referentie naar auteur. 
	 * De taal heeft de standaardwaarde Nederlands.
	 * @param titel de titel van het boek
	 * @param auteur de auteur van het boek
	 */
	public Boek (String titel, Auteur auteur) {
		this.titel = titel;
		this.auteur = auteur;
		this.taal = "Nederlands";
	}
	
	/**
	 * Levert de naam van het boek.
	 * @return de titel van het boek
	 */
	public String getTitel() {
		return titel;
	}

	/**
	 * Haalt object auteur op
	 * @return object auteur
	 */
	public Auteur getAuteur() {
		return auteur;
	}
	
	/**
	 * Levert de taal van het boek op.
	 * @return de taal van het boek
	 */
	public String getTaal() {
		return taal;
	}
	 

}
