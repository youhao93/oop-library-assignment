package domein;

/**
 * Klasse die verantwoordelijk is voor het creeeren van auteurs.
 * @author youhao
 */

public class Auteur {
	private String naam;
	private boolean literairePrijs = false;
	
	/**
	 * Maakt een nieuwe Auteur aan met gegeven naam en literaire
	 * prijs.
	 * @param naam de naam van de auteur
	 * @param literairePrijs de prijs die de auteur of wel of niet heeft.
	 */
	public Auteur (String naam, boolean literairePrijs) {
		this.naam = naam;
		this.literairePrijs = literairePrijs;
	}
	/**
	 * Levert de naam van de auteur.
	 * @return de naam van de auteur
	 */
	public String getNaam () {
		return naam;
	}
	
	/**
	 * Geeft als antwoord terug of er wel of geen prijs is.
	 * @return de wel of geen prijs van de auteur
	 */
	public boolean getPrijs () {
		return literairePrijs;
	}
}
