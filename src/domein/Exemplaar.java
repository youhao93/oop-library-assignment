package domein;


/**
 * Klasse die verantwoordelijk is voor het generen van nieuwe exemplaren 
 * en kopies van exemplaren.
 * Dit is een subklasse van de superklasse boek.
 * @author youhao
 *
 */
public class Exemplaar {
	private Boek exemplaarVanBoek;

	/**
	 * Maakt een nieuw exemplaar met als parameter het boek waar een exemplaar
	 * van wordt gemaakt.
	 * @param exemplaarVanBoek is het boek waar een exemplaar van wordt gemaakt
	 */
	public Exemplaar(Boek exemplaarVanBoek) {
		this.exemplaarVanBoek = exemplaarVanBoek;
		System.out.println("Nieuw exemplaar --> " + exemplaarVanBoek.getTitel() +
		"van " + exemplaarVanBoek.getAuteur().getNaam() + " in taal " + exemplaarVanBoek.getTaal());
	}
	
	/**
	 * Maakt een kopie van een exemplaar met gegeven een Exemplaar waar een kopie 
	 * van wordt gemaakt.
	 * @param kopieVanExemplaar is het exemplaar waar een kopie van wordt gemaakt
	 */
	public Exemplaar(Exemplaar kopieVanExemplaar) {
		this.exemplaarVanBoek = kopieVanExemplaar.exemplaarVanBoek;
		System.out.println("Kopie van exemplaar --> " + kopieVanExemplaar.exemplaarVanBoek.getTitel() +
		" van " + kopieVanExemplaar.exemplaarVanBoek.getAuteur().getNaam() + " in taal " + 
				kopieVanExemplaar.exemplaarVanBoek.getTaal());;
	}
	
	/**
	 * Gegevens van het boek, titel, auteur en taal worden afgedrukt.
	 */
	public void print () {
		System.out.println("Exemplaar " + exemplaarVanBoek.getTitel() + " van " + exemplaarVanBoek.getAuteur().getNaam() +
				" in taal " + exemplaarVanBoek.getTaal());
	}
	
	public Boek getExemplaarVanBoek() {
		return exemplaarVanBoek;
	}
	

}
