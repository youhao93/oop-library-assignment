package domein;

import java.util.ArrayList;;

/**
 * Klasse die verantwoordelijk is voor het beheer van boeken.
 * @author youhao
 *
 */
public class Bibliotheek {
	
	private ArrayList<Exemplaar> boekenLijst = new ArrayList<>();

	/**
	 * Constructor die na creatie van bibliotheek uitprint dat 
	 * de bibliotheek is geopend
	 * @param naam prints de naam van de net gecreeerde bibliotheek uit
	 */
	public Bibliotheek (String naam) {
		System.out.println(naam + " is geopend!");
	}
	
	/**
	 * Levert een melding op wanneer een boek is gecreerd.
	 */
	public void voegToe(Boek boek) {
		boekenLijst.add(new Exemplaar(boek));
	}

	/**
	 * Voegt 1 of meer boeken aan de boekenlijst.
	 * @param boek is de boek die aan de boekenlijst wordt toegevoegd
	 * @param aantal is het aantal exemplaren van boek dat aan de boekenlijst wordt toegevoegd
	 */
	public void voegToe(Boek boek, int aantal) {
		for (int i = 0; i < aantal; i++) {
			boekenLijst.add(new Exemplaar(boek));
		}
	}

	/**
	 * Levert een overzicht aan exemplaren op die in dezelfde taal is geschreven
	 * als in de parameter. 
	 * @param taal is de geschreven taal van de boeken.
	 * @return een overzicht van de boeken uit de boekenlijst die in 
	 * dezelfde taal is geschreven
	 */
	
	public void toonCollectie(String taal){
		for (Exemplaar exemplaar : boekenLijst) {
			if (exemplaar.getExemplaarVanBoek().getTaal().equals(taal)) {
				System.out.println("exemplaar --> " + exemplaar.getExemplaarVanBoek().getTitel() + " van " + 
			exemplaar.getExemplaarVanBoek().getAuteur().getNaam() + " in taal " + exemplaar.getExemplaarVanBoek().getTaal());
			}
		}
	}
	
	
	/**
	 * Toont alle exemplaren van de boekenlijst
	 * @return levert een overzicht op van alle exemplaren uit de boekenlijst
	 */
	public void toonCollectie(){
		for (Exemplaar exemplaar : boekenLijst) {
			System.out.println("exemplaar --> " + exemplaar.getExemplaarVanBoek().getTitel() 
			+ " van " + exemplaar.getExemplaarVanBoek().getAuteur().getNaam() + " in taal " 
			+ exemplaar.getExemplaarVanBoek().getTaal());
		}
	}

	/**
	 * Print alle auteurs met prijs
	 */
	public void printAuteurs() {
		for (Exemplaar exemplaar : boekenLijst) {
			boolean prijsAuteur = exemplaar.getExemplaarVanBoek().getAuteur().getPrijs();
			if (prijsAuteur == true) {
				System.out.println(exemplaar.getExemplaarVanBoek().getAuteur().getNaam());
			}
		}
	}
	
	/**
	 * Print alle auteurs die wel of geen prijs hebben
	 * @param prijsWelOfGeenPrijs geeft aan of de auteur wel of geen prijs heeft
	 */
	public void printAuteurs(boolean prijsWelOfGeenPrijs) {	
		for (Exemplaar exemplaar : boekenLijst) {
			if (prijsWelOfGeenPrijs == true) {
				System.out.println(exemplaar.getExemplaarVanBoek().getAuteur().getNaam());
			} else {
				System.out.println(exemplaar.getExemplaarVanBoek().getAuteur().getNaam());
			}
		}
	}
	
	/**
	 * Telt aantal exemplaren op van een boek
	 * @param boek is het boek waarvan het aantal exemplaren wordt opgeteld
	 * @return de terugkeerwaarde van het aantal opgetelde exemplaren
	 */
	public int telExemplaren (Boek boek) {
		int aantalExemplaren = 0;
		for (Exemplaar exemplaar : boekenLijst) {
			if (exemplaar.getExemplaarVanBoek().getTitel().equals(boek.getTitel())) {
				aantalExemplaren++;
			}
		}
		return aantalExemplaren;
	}
}
