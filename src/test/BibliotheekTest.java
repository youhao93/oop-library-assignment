package test;

import domein.Bibliotheek;

import domein.Auteur;
import domein.Boek;

/**
 * Testklasse die de methode telExemplaren test door te zoeken op basis van 
 * de titel.
 * @author youhao
 *
 */
	public class BibliotheekTest {
		public static void main (String[] args) {
		    Bibliotheek testBieb = new Bibliotheek("Testbibliotheek");
		    
			Auteur a1 = new Auteur("Piet", true);
		    Auteur a2 = new Auteur("Jan", true);
		    Auteur a3 = new Auteur("Kees", false);
		    Auteur a4 = new Auteur("Herman", false);
		    
		    Boek b1 = new Boek("Ik ben een titel van een boek ", "Nederlands", a1);
		    Boek b2 = new Boek("De vurige landschappen van vuurland ", a1);
		    Boek b3 = new Boek("Olympus, de plek van goden ", a2);
		    Boek b4 = new Boek("Hermes is een griekse God ", a2);
		    Boek b5 = new Boek("De drie biggetjes ", "Engels", a3);
		    Boek b6 = new Boek("Het verhaal van de reiziger ", a4);
			
		    testBieb.voegToe(b1, 6);
		    System.out.println();
		    testBieb.voegToe(b2, 2);
		    System.out.println();
		    testBieb.voegToe(b2, 8);
		    System.out.println();
		    testBieb.voegToe(b3, 2);
		    System.out.println();
		    testBieb.voegToe(b4, 4);
		    System.out.println();
		    testBieb.voegToe(b5, 2);
		    System.out.println();
		    testBieb.voegToe(b6, 10);
		    System.out.println();
		    
			System.out.println("Test aantal exemplaren " + 
			testBieb.telExemplaren(b1) + " van " + b1.getTitel());
			System.out.println("Test aantal exemplaren " + 
			testBieb.telExemplaren(b2) + " van " + b2.getTitel());
			System.out.println("Test aantal exemplaren " + 
			testBieb.telExemplaren(b3) + " van " + b3.getTitel());
			System.out.println("Test aantal exemplaren " + 
			testBieb.telExemplaren(b4) + " van " + b4.getTitel());
			System.out.println("Test aantal exemplaren " + 
			testBieb.telExemplaren(b5) + " van " + b5.getTitel());
			System.out.println("Test aantal exemplaren " + 
			testBieb.telExemplaren(b6) + " van " + b6.getTitel());
		}
	}